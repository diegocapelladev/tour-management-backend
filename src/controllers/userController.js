import User from '../models/User'

export const updateUser = async (req, res) => {
  const id = req.params.id

  try {
    const updateUser = await User.findByIdAndUpdate(
      id,
      {
        $set: req.body
      },
      {
        new: true
      }
    )

    res.status(200).json({
      success: true,
      message: 'Successfully updated.',
      data: updateUser
    })
  } catch (err) {
    res
      .status(400)
      .json({ success: false, message: 'Failure to update. Try again.' })
  }
}

export const deleteUser = async (req, res) => {
  const id = req.params.id

  try {
    await User.findByIdAndDelete(id)

    res.status(200).json({
      success: true,
      message: 'Successfully deleted.'
    })
  } catch (err) {
    res
      .status(500)
      .json({ success: false, message: 'Failure to delete. Try again.' })
  }
}

export const getSingleUser = async (req, res) => {
  const id = req.params.id

  try {
    const user = await User.findById(id)

    res.status(200).json({
      success: true,
      message: 'Successful.',
      data: user
    })
  } catch (err) {
    res.status(404).json({ success: false, message: 'Not Found. Try again.' })
  }
}

export const getAllUser = async (req, res) => {
  try {
    const user = await User.find({})
    res.status(200).json({
      success: true,
      message: 'Successful',
      data: user
    })
  } catch (err) {
    res.status(404).json({ success: false, message: 'Not Found. Try again.' })
  }
}
