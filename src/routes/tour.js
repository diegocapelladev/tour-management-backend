import { Router } from 'express'
import {
  createTour,
  updateTour,
  deleteTour,
  getSingleTour,
  getAllTour,
  getTourBySearch,
  getFeaturedTour,
  getTourCount
} from '../controllers/tourController'
import { verifyAdmin } from '../utils/verifyToken'

const routes = Router()

routes.post('/', verifyAdmin, createTour)

routes.put('/:id', verifyAdmin, updateTour)

routes.delete('/:id', verifyAdmin, deleteTour)

routes.get('/:id', getSingleTour)

routes.get('/', getAllTour)

routes.get('/search/getTourBySearch', getTourBySearch)

routes.get('/search/getFeaturedTour', getFeaturedTour)

routes.get('/search/getTourCount', getTourCount)

export default routes
