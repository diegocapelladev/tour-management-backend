import { Router } from 'express'
import {
  createBooking,
  getAllBooking,
  getBooking
} from '../controllers/bookingController'
import { verifyAdmin, verifyUser } from '../utils/verifyToken'

const routes = Router()

routes.post('/', verifyUser, createBooking)

routes.get('/:id', verifyUser, getBooking)

routes.get('/', verifyAdmin, getAllBooking)

export default routes
