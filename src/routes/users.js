import { Router } from 'express'
import {
  deleteUser,
  getAllUser,
  getSingleUser,
  updateUser
} from '../controllers/userController'
import { verifyAdmin, verifyUser } from '../utils/verifyToken'

const routes = Router()

routes.put('/:id', verifyUser, updateUser)

routes.delete('/:id', verifyUser, deleteUser)

routes.get('/:id', verifyUser, getSingleUser)

routes.get('/', verifyAdmin, getAllUser)

export default routes
