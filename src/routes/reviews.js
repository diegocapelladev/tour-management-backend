import { Router } from 'express'
import { createReview } from '../controllers/reviewController'
import { verifyUser } from '../utils/verifyToken'

const routes = Router()

routes.post('/:tourId', verifyUser, createReview)

export default routes
