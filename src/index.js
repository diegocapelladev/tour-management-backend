import express from 'express'
import dotenv from 'dotenv'
import cors from 'cors'
import cookieParser from 'cookie-parser'
import mongoose from 'mongoose'

import tourRoutes from './routes/tour'
import userRoutes from './routes/users'
import authRoutes from './routes/auth'
import reviewRoutes from './routes/reviews'
import bookingRoutes from './routes/bookings'

dotenv.config()

const app = express()

const port = process.env.PORT || 3001

const corsOptions = {
  origin: true,
  credentials: true
}

mongoose.set('strictQuery', true)
const connect = async () => {
  try {
    await mongoose.connect(process.env.MONGO, {
      useNewUrlParser: true,
      useUnifiedTopology: true
    })

    console.log('MongoDB database connected!')
  } catch (err) {
    console.log('MongoDB database connection failed: ', err)
  }
}

app.use(express.json())
app.use(cors(corsOptions))
app.use(cookieParser())

app.use('/api/v1/auth', authRoutes)
app.use('/api/v1/tours', tourRoutes)
app.use('/api/v1/users', userRoutes)
app.use('/api/v1/review', reviewRoutes)
app.use('/api/v1/booking', bookingRoutes)

app.listen(port, () => {
  connect()
  console.log('Server is running on port:', port)
})
