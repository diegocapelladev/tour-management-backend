# Tour Management Back-end API

## Getting Started

### Set ENV
PORT=3001
MONGO=
JTW_SECRET_KEY=

### RUN

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3001/api/v1](http://localhost:3001/api/v1) with your browser to see the result.

The FRONT-END can be accessed on [https://gitlab.com/diegocapelladev/tour-management](https://gitlab.com/diegocapelladev/tour-management)
